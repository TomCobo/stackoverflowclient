//
//  QuestionServiceResponseMappingTests.m
//  StackoverflowClient
//
//
//  QuestionDataServiceTests.m
//  StackoverflowClient
//
//  Created by Tomas Cobo on 23/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "QuestionServiceResponseMappingTests.h"
#import "QuestionServiceResponseObjectMapping.h"
#import "QuestionsDataService.h"
#import "Question.h"
#import "QuestionOwner.h"
#import "RKMappingTest+RKTestingExampleExpectations.h"

@interface QuestionServiceResponseMappingTests()

@property (nonatomic) id parsedJSON;
@property (nonatomic) RKMappingTest *mappingTest;
@property (nonatomic) Question* question;

@end

@implementation QuestionServiceResponseMappingTests

static NSString *assertErrorPropertyNotSetup = @"This property has not been setup";

-(Question *)question {
    if (_question == nil) {
        _question = [Question new];
        
        QuestionOwner* owner = [QuestionOwner new];
        owner.reputation = @9;
        owner.imageUrl = @"https://www.gravatar.com/avatar/05ab26575f54092b96733db250fee809?s=128&d=identicon&r=PG&f=1";
        owner.name = @"mrboyd";
        owner.link = @"http://stackoverflow.com/users/5965969/mrboyd";
        
        _question.owner = owner;
        _question.tags = @[@"c",@"swift",@"memory-management",@"c-strings",@"bridging-header"];
        _question.views = 27;
        _question.answers = 0;
        _question.votes = 1;
        _question.creationDate = 1456191827;
        _question.link = @"http://stackoverflow.com/questions/35567288/mallocd-c-string-created-in-c-file-being-lost-when-bridging-to-swift-file-on-sp";
        _question.title = @"malloc&#39;d C string created in c file being lost when bridging to swift file on specific input";
    }
    return _question;
}

- (void)setUp {
    [super setUp];
    
    //  Fixture
    NSBundle *testTargetBundle = [NSBundle bundleWithIdentifier:@"company.StackoverflowClientTests"];
    RKObjectMapping* questionsMapping = [QuestionServiceResponseObjectMapping new].objectMapping;
    [RKTestFixture setFixtureBundle:testTargetBundle];
    
    //  Mapping JSON
    self.parsedJSON = [RKTestFixture parsedObjectWithContentsOfFixture:@"questions.json"];
    self.mappingTest = [RKMappingTest testForMapping:questionsMapping sourceObject:self.parsedJSON destinationObject:nil];
    
}

- (void)tearDown {
    [super tearDown];
    self.parsedJSON = nil;
    self.mappingTest = nil;
}

- (void)testQuestionsDataServiceAttributeMax {
    [self checkPropertyWithPath:@"quota_max" andKeyPath:@"max"];
}

- (void)testQuestionsDataServiceAttributeRemaining {
    [self checkPropertyWithPath:@"quota_remaining" andKeyPath:@"remaining"];
}

- (void)testQuestionsDataServiceAttributeOwner {
    [self checkPropertyWithPath:@"owner" andKeyPath:@"owner"];
}

- (void)testQuestionsDataServiceAttributeItems {
    [self checkPropertyWithPath:@"items" andKeyPath:@"items"];
}

- (void)testQuestionsDataServiceAttributeItemsCountIsEqualToThirty {
    [self.mappingTest expectMappingFromKeyPath:@"items" toCollectionAtKeyPath:@"items" withCount:30];
    XCTAssertTrue([self.mappingTest evaluate], @"Items should be 30");
}

- (void)testQuestionsDataServiceFirstItemIsAQuestion {
    [self.mappingTest expectMappingFromKeyPath:@"items" toCollectionAtKeyPath:@"items" withFirstElementQuestion:[Question class]];
    XCTAssertTrue([self.mappingTest evaluate], @"First Items should be a question");
}

- (void)testQuestionsDataServiceFirstQuestionHasRightFirstQuestion {
    [self.mappingTest expectMappingFromKeyPath:@"items" toCollectionAtKeyPath:@"items" withQuestion:self.question];
    XCTAssertTrue([self.mappingTest evaluate], @"First Items should be a question");
}

-(void)checkPropertyWithPath:(NSString*)path andKeyPath:(NSString*)keypath{
    [self.mappingTest addExpectation:[RKPropertyMappingTestExpectation expectationWithSourceKeyPath:path destinationKeyPath:keypath]];
    XCTAssertTrue([self.mappingTest evaluate], @"%@ property has not been setup", keypath);
}


@end

