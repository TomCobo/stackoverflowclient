//
//  SearchParmsTests.m
//  StackoverflowClient
//
//  Created by Tomas Cobo on 24/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "SearchParmsTests.h"
#import "SearchParams.h"

@interface SearchParmsTests()
@property(nonatomic) SearchParams* searchParams;
@end

@implementation SearchParmsTests

- (void)setUp {
    [super setUp];
    
    self.searchParams = [SearchParams new];
    
}

- (void)tearDown {
    [super tearDown];
    self.searchParams = nil;
}

-(void)testSearchParamsTagsReparatedByCommas {
    self.searchParams.tagsString = @"C,C++";
    XCTAssertTrue([self.searchParams.tagsString isEqualToString:@"C;C++"], @"Tags param not created properly.");
}

-(void)testSearchParamsSpacesAreReplaced {
    self.searchParams.tagsString = @"C, C++";
    XCTAssertTrue([self.searchParams.tagsString isEqualToString:@"C;C++"], @"Tags param did not replace spaces");
}

@end
