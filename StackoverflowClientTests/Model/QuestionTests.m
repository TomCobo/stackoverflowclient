//
//  QuestionTests.m
//  StackoverflowClient
//
//  Created by T Cobo Martinez on 23/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "QuestionTests.h"
#import "Question.h"

@interface QuestionTests()
@property (nonatomic) Question* question;
@end

@implementation QuestionTests

- (void)setUp {
    [super setUp];
    self.question = [Question new];
    
    QuestionOwner* owner = [QuestionOwner new];
    owner.reputation = @9;
    owner.imageUrl = @"https://www.gravatar.com";
    owner.name = @"mrboyd";
    owner.link = @"http://stackoverflow.com/users";
    
    self.question.owner = owner;
    self.question.tags = @[@"c",@"swift",@"memory-management"];
    self.question.views = 27;
    self.question.answers = 0;
    self.question.votes = 1;
    self.question.creationDate = 1456191827;
    self.question.link = @"http://stackoverflow.com/questions/";
    self.question.title = @"lost when bridging to swift file on specific input";
}

- (void)testQuestionIsEqualObject {
    
    Question* question = [Question new];
    
    QuestionOwner* owner = [QuestionOwner new];
    owner.reputation = @9;
    owner.imageUrl = @"https://www.gravatar.com";
    owner.name = @"mrboyd";
    owner.link = @"http://stackoverflow.com/users";
    
    question.owner = owner;
    question.tags = @[@"c",@"swift",@"memory-management"];
    question.views = 27;
    question.answers = 0;
    question.votes = 1;
    question.creationDate = 1456191827;
    question.link = @"http://stackoverflow.com/questions/";
    question.title = @"lost when bridging to swift file on specific input";

    XCTAssertTrue([self.question isEqualToQuestion:question],@"These two questions should be equals.");

}

@end
