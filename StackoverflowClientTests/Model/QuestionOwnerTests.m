//
//  QuestionOwnerTests.m
//  StackoverflowClient
//
//  Created by T Cobo Martinez on 24/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "QuestionOwnerTests.h"
#import "QuestionOwner.h"

@interface QuestionOwnerTests()
@property (nonatomic) QuestionOwner* owner;
@end

@implementation QuestionOwnerTests

- (void)setUp {
    [super setUp];
    
    self.owner = [QuestionOwner new];
    self.owner.reputation = @9;
    self.owner.imageUrl = @"https://www.gravatar.com";
    self.owner.name = @"mrboyd";
    self.owner.link = @"http://stackoverflow.com/users";
}

- (void)testQuestionIsEqualObject {

    QuestionOwner* owner = [QuestionOwner new];
    owner.reputation = @9;
    owner.imageUrl = @"https://www.gravatar.com";
    owner.name = @"mrboyd";
    owner.link = @"http://stackoverflow.com/users";
    
    XCTAssertTrue([self.owner isEqualToQuestionOwner:owner],@"These two question authors should be equals.");
    
}

@end

