//
//  RKMappingTest+RKTestingExampleExpectations.m
//  StackoverflowClient
//
//  Created by T Cobo Martinez on 23/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "RKMappingTest+RKTestingExampleExpectations.h"
#import "Question.h"

@implementation RKMappingTest (RKTestingExampleExpectations)

- (void)expectMappingFromKeyPath:(NSString *)sourceKeyPath toCollectionAtKeyPath:(NSString *)destinationKeyPath {
    [self addExpectation:[RKPropertyMappingTestExpectation expectationWithSourceKeyPath:sourceKeyPath destinationKeyPath:destinationKeyPath evaluationBlock:^BOOL(RKPropertyMappingTestExpectation *expectation, RKPropertyMapping *mapping, id mappedValue, NSError **error) {
        return [mappedValue isKindOfClass:[NSArray class]] || [mappedValue isKindOfClass:[NSSet class]];
    }]];
}

- (void)expectMappingFromKeyPath:(NSString *)sourceKeyPath toCollectionAtKeyPath:(NSString *)destinationKeyPath withCount:(NSUInteger)count {
    [self addExpectation:[RKPropertyMappingTestExpectation expectationWithSourceKeyPath:sourceKeyPath destinationKeyPath:destinationKeyPath evaluationBlock:^BOOL(RKPropertyMappingTestExpectation *expectation, RKPropertyMapping *mapping, id mappedValue, NSError **error) {
        return ([mappedValue isKindOfClass:[NSArray class]] || [mappedValue isKindOfClass:[NSSet class]]) && [(NSArray *)mappedValue count] == count;
    }]];
}

- (void)expectMappingFromKeyPath:(NSString *)sourceKeyPath toCollectionAtKeyPath:(NSString *)destinationKeyPath withFirstElementQuestion:(Class)classToCompare {
    [self addExpectation:[RKPropertyMappingTestExpectation expectationWithSourceKeyPath:sourceKeyPath destinationKeyPath:destinationKeyPath evaluationBlock:^BOOL(RKPropertyMappingTestExpectation *expectation, RKPropertyMapping *mapping, id mappedValue, NSError **error) {
        return ([mappedValue isKindOfClass:[NSArray class]] && [((NSArray*)mappedValue).firstObject isKindOfClass:classToCompare] );
    }]];
}

- (void)expectMappingFromKeyPath:(NSString *)sourceKeyPath toCollectionAtKeyPath:(NSString *)destinationKeyPath withQuestion:(Question*)question {
    [self addExpectation:[RKPropertyMappingTestExpectation expectationWithSourceKeyPath:sourceKeyPath destinationKeyPath:destinationKeyPath evaluationBlock:^BOOL(RKPropertyMappingTestExpectation *expectation, RKPropertyMapping *mapping, id mappedValue, NSError **error) {
        
        BOOL hasSameValues = NO;
        if ([((NSArray*)mappedValue).firstObject isKindOfClass:[Question class]]) {
            Question* firstQuestion = ((NSArray*)mappedValue).firstObject;
            hasSameValues = [firstQuestion isEqualToQuestion:question];
        }
        
        return hasSameValues;
    }]];
}


@end
