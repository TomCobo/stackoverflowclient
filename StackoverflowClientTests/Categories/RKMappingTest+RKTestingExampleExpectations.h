//
//  RKMappingTest+RKTestingExampleExpectations.h
//  StackoverflowClient
//
//  Created by T Cobo Martinez on 23/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <RestKit/Testing.h>

@class Question;

@interface RKMappingTest (RKTestingExampleExpectations)

- (void)expectMappingFromKeyPath:(NSString *)sourceKeyPath toCollectionAtKeyPath:(NSString *)destinationKeyPath;

- (void)expectMappingFromKeyPath:(NSString *)sourceKeyPath toCollectionAtKeyPath:(NSString *)destinationKeyPath withCount:(NSUInteger)count;
- (void)expectMappingFromKeyPath:(NSString *)sourceKeyPath toCollectionAtKeyPath:(NSString *)destinationKeyPath withFirstElementQuestion:(Class)classToCompare;
- (void)expectMappingFromKeyPath:(NSString *)sourceKeyPath toCollectionAtKeyPath:(NSString *)destinationKeyPath withQuestion:(Question*)question;

@end
