//
//  NSString+DateFormating.m
//  StackoverflowClient
//
//  Created by T Cobo Martinez on 23/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "NSString+DateFormating.h"

@implementation NSString (DateFormating)


+(NSString*)friendlyDateFromTimeStamp:(NSString*)timeStamp {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [NSString dateStringFromDate:[dateFormatter dateFromString:timeStamp]];
}

+(NSString*)dateStringFromDate:(NSDate*) date {
    return [self friendlyDateFromDate:date andCurrentTimeIntervalInSeconds:[NSNumber numberWithDouble:[date timeIntervalSinceNow]]];
}

+(NSString*)friendlyDateFromDate:(NSDate*)date andCurrentTimeIntervalInSeconds:(NSNumber*)timeInterval {
    
    // Use (-) modifier as date is earlier than now
    double secondsPassed = -[timeInterval doubleValue];
    
    NSString *friendlyDate;
    
    static double secondsInMinute = 60;
    static double secondsInHour = 3600;
    static double secondsInDay = 24*3600;
    
    if (secondsPassed < 9 * secondsInMinute) {
        friendlyDate = @"Just now";
    } else if (secondsPassed < 14 * secondsInMinute) {
        friendlyDate = @"10 mins ago";
    } else if (secondsPassed < 18 * secondsInMinute) {
        friendlyDate = @"15 mins ago";
    } else if (secondsPassed < 25 * secondsInMinute) {
        friendlyDate = @"20 mins ago";
    } else if (secondsPassed < 35 * secondsInMinute) {
        friendlyDate = @"30 mins ago";
    } else if (secondsPassed < 45 * secondsInMinute) {
        friendlyDate = @"40 mins ago";
    } else if (secondsPassed < 90 * secondsInMinute) {
        friendlyDate = @"1 hour ago";
    }
    else if (secondsPassed < secondsInHour * 18) {
        double approxHoursPassed = secondsPassed / (secondsInHour);
        int roundedHoursPassed = (approxHoursPassed + 0.5);
        friendlyDate = [NSString stringWithFormat:@"%i hours ago", roundedHoursPassed];
    }
    else if (secondsPassed < secondsInHour * 36) {
        friendlyDate = @"1 day ago";
    }
    else if (secondsPassed < secondsInDay * 7) {
        //7 days ago
        double approxDaysPassed = secondsPassed / (secondsInDay);
        int roundedDaysPassed = (approxDaysPassed + 0.5);
        friendlyDate = [NSString stringWithFormat:@"%i days ago", roundedDaysPassed];
    }
    else if (secondsPassed < secondsInDay * 28) {
        double approxDaysPassed = secondsPassed / (secondsInDay);
        
        if (approxDaysPassed<=10.5) {
            friendlyDate = @"1 week ago";
        }
        else {
            friendlyDate = [NSString stringWithFormat:@"%i weeks ago", (int)((approxDaysPassed / 7) + 0.5)];
        }
    }
    else if (secondsPassed < (secondsInDay * 345)) {
        double approxDaysPassed = secondsPassed / (secondsInDay);
        
        if (approxDaysPassed <=45) {
            friendlyDate = @"1 month ago";
        }
        else {
            friendlyDate = [NSString stringWithFormat:@"%i months ago", (int) ((approxDaysPassed/30)+0.5)];
        }
    }
    else {
        double approxDaysPassed = secondsPassed / (secondsInDay);
        int approxYearsPassed = (int)((approxDaysPassed / 365)+0.5);
        
        if (approxDaysPassed < (365 * 1.5)) {
            friendlyDate = @"1 year ago";
        }else{
            friendlyDate = [NSString stringWithFormat:@"%i years ago", approxYearsPassed];
        }
    }
    
    return friendlyDate;
}


@end
