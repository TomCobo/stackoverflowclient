//
//  NSString+DateFormating.h
//  StackoverflowClient
//
//  Created by T Cobo Martinez on 23/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (DateFormating)

+(NSString*)friendlyDateFromTimeStamp:(NSString*)timeStamp;

@end
