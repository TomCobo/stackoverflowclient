//
//  UIScreen+ScreenSize.h
//  StackoverflowClient
//
//  Created by T Cobo Martinez on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIScreen (ScreenSize)

+(CGSize) sizeWithCurrentOrientation;

@end
