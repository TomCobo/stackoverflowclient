//
//  HttpRequetsManager.h
//  StackoverflowClient
//
//  Created by Tomas Cobo on 24/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HttpRequetsManager : NSObject

- (void)requestWithURL:(NSURL*)url andResponseDescriptor:(NSArray*)responseDescriptors completionBlock:(void(^)(NSError * error, id response)) completedBlock;

@end
