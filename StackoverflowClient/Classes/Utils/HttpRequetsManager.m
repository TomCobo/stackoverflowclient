//
//  HttpRequetsManager.m
//  StackoverflowClient
//
//  Created by Tomas Cobo on 24/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "HttpRequetsManager.h"

@implementation HttpRequetsManager

- (void)requestWithURL:(NSURL*)url andResponseDescriptor:(NSArray*)responseDescriptors completionBlock:(void(^)(NSError * error, id response)) completedBlock {
    RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:[NSURLRequest requestWithURL:url] responseDescriptors:responseDescriptors];
    
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        completedBlock(nil, [result firstObject]);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        completedBlock(error, nil);
    }];
    
    [operation start];
}

@end
