//
//  StackClientCollectionView.h
//  StackoverflowClient
//
//  Created by Tomas Cobo on 23/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "BaseView.h"
#import "StackClientDataSource.h"

@interface StackClientCollectionView : BaseView

- (instancetype)initWithDataSource:(StackClientDataSource*)dataSource;

-(void)refresh;

@end
