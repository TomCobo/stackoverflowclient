//
//  StackClientCollectionView.m
//  StackoverflowClient
//
//  Created by Tomas Cobo on 23/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "StackClientCollectionView.h"
#import "UIScreen+ScreenSize.h"

@interface StackClientCollectionView() {
    UICollectionView* _collectionView;
    StackClientDataSource* _dataSource;
}

@end

@implementation StackClientCollectionView

- (instancetype)initWithDataSource:(StackClientDataSource*)dataSource {
    self = [super init];
    if (self) {
        _dataSource = dataSource;
    }
    return self;
}

#pragma mark - setup subviews

-(void)setupSubviews {
    [self addSubview:self.collectionView];
}

-(void)setupAutolayout {
    [self.collectionView alignTop:@"0" leading:@"0" bottom:@"0" trailing:@"0" toView:self];
}

#pragma mark - subviews

- (UICollectionView*) collectionView {
    if (_collectionView == nil) {
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.collectionFlowLayout];
        _collectionView.backgroundColor = [UIColor grayColor];
        _collectionView.alpha = 0.0;
    }
    return _collectionView;
}

#pragma mark - layout

- (UICollectionViewFlowLayout*) collectionFlowLayout{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake([UIScreen sizeWithCurrentOrientation].width - 40, 160);
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    flowLayout.minimumInteritemSpacing = 20;
    flowLayout.sectionInset = UIEdgeInsetsMake(20, 0, 0, 0);
    return flowLayout;
}

#pragma mark - dataSource

-(StackClientDataSource*) dataSource {
    return _dataSource;
}

#pragma mark - public methods

-(void)refresh {
    
    [self.collectionView registerClass:self.dataSource.cellClass forCellWithReuseIdentifier:@"Cell"];
    self.collectionView.dataSource = self.dataSource;
    
    [self.collectionView reloadData];
    [UIView animateWithDuration:0.6 animations:^{
        self.collectionView.alpha = 1.0;
    }];

}

@end
