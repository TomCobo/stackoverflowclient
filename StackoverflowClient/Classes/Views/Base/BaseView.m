//
//  BaseView.m
//  StackoverflowClient
//
//  Created by Tomas Cobo on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "BaseView.h"

@implementation BaseView


- (instancetype)init {
    self = [super init];
    if (self) {
        [self setupSubviews];
        [self setupAutolayout];
    }
    return self;
}

-(void)setupSubviews {
    
}

-(void)setupAutolayout {
    
}

@end
