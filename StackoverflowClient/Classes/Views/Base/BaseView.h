//
//  BaseView.h
//  StackoverflowClient
//
//  Created by Tomas Cobo on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+FLKAutoLayout.h"

@interface BaseView : UIView

-(void)setupSubviews;
-(void)setupAutolayout;

@end
