//
//  Question.m
//  StackoverflowClient
//
//  Created by T Cobo Martinez on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "Question.h"
#import "NSString+DateFormating.h"

@implementation Question

-(NSString *)friendlyCreationDate {
    return [NSString friendlyDateFromTimeStamp:[NSString stringWithFormat:@"%ld",(long)self.creationDate]];
}

- (BOOL)isEqualToQuestion:(Question *)question {
    if (!question) {
        return NO;
    }
    
    BOOL hasSameValues = NO;
    
    if ([question isKindOfClass:[self class]]) {
        hasSameValues = [self.title isEqualToString:question.title] && (self.votes == question.votes) && (self.answers == question.answers) && (self.views == question.views) && [self.link isEqualToString:question.link] && [self.tags isEqualToArray:question.tags] && [self.owner isEqualToQuestionOwner:question.owner];
    }
    return hasSameValues;
}

@end
