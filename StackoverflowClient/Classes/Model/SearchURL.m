//
//  SearchURL.m
//  StackoverflowClient
//
//  Created by T Cobo Martinez on 24/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "SearchURL.h"

static NSString *const baseURL = @"https://api.stackexchange.com/2.2/";

@interface SearchURL()

@property (nonatomic) NSString* endPoint;
@property (nonatomic) SearchParams* params;

@end

@implementation SearchURL

- (instancetype)initWithEndpoint:(NSString*)endpoint andSearhParams:(SearchParams*)params {
    self = [super init];
    if (self) {
        self.params = params;
        self.endPoint = endpoint;
    }
    return self;
}


- (NSURL*)findURL {
    
    NSMutableString* urlMutableString = [NSMutableString stringWithFormat:@"%@%@%@",baseURL,self.endPoint,@"?site=stackoverflow&sort=hot"];
    
    if (self.params.tagsString != nil) {
        [urlMutableString appendFormat:@"&tagged=%@",self.params.tagsString];
    }
    return [NSURL URLWithString:[NSString stringWithString:urlMutableString]];
}

@end
