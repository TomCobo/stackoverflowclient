//
//  QuestionOwner.m
//  StackoverflowClient
//
//  Created by T Cobo Martinez on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "QuestionOwner.h"

@implementation QuestionOwner

- (BOOL)isEqualToQuestionOwner:(QuestionOwner *)owner {
    if (!owner) {
        return NO;
    }
    
    BOOL hasSameValues = NO;
    if ([owner isKindOfClass:[self class]]) {
        hasSameValues = [self.name isEqualToString:owner.name] && [self.imageUrl isEqualToString:owner.imageUrl] && [self.reputation isEqualToNumber:owner.reputation] && [self.link isEqualToString:owner.link];
    }
    return hasSameValues;
}

@end
