//
//  SearchParams.h
//  StackoverflowClient
//
//  Created by Tomas Cobo on 23/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchParams : NSObject

@property (nonatomic) NSString *tagsString;

@end
