//
//  Question.h
//  StackoverflowClient
//
//  Created by T Cobo Martinez on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QuestionOwner.h"

@interface Question : NSObject

@property (nonatomic) NSString *title;
@property (nonatomic) NSInteger votes;
@property (nonatomic) NSInteger answers;
@property (nonatomic) NSInteger views;

@property (nonatomic) NSInteger creationDate;
@property (nonatomic) QuestionOwner *owner;
@property (nonatomic) NSArray* tags;
@property (nonatomic) NSString *link;

@property (nonatomic) NSString* friendlyCreationDate;

- (BOOL)isEqualToQuestion:(Question *)question;

@end
