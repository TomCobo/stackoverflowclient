//
//  SearchURL.h
//  StackoverflowClient
//
//  Created by T Cobo Martinez on 24/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SearchParams.h"

@interface SearchURL: NSObject

- (instancetype)initWithEndpoint:(NSString*)endpoint andSearhParams:(SearchParams*)params;
- (NSURL*)findURL;

@end
