//
//  QuestionOwner.h
//  StackoverflowClient
//
//  Created by T Cobo Martinez on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuestionOwner : NSObject

@property (nonatomic) NSString *name;
@property (nonatomic) NSString *imageUrl;
@property (nonatomic) NSNumber *reputation;
@property (nonatomic) NSString *link;

- (BOOL)isEqualToQuestionOwner:(QuestionOwner *)owner;

@end
