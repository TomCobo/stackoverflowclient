//
//  SearchParams.m
//  StackoverflowClient
//
//  Created by Tomas Cobo on 23/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "SearchParams.h"

@implementation SearchParams

-(void)setTagsString:(NSString *)tagsString {
    _tagsString = [[tagsString stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"," withString:@";"];
}

@end
