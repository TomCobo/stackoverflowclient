//
//  QuestionServiceResponseObjectMapping.m
//  StackoverflowClient
//
//  Created by T Cobo Martinez on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "QuestionServiceResponseObjectMapping.h"
#import "QuestionServiceResponse.h"
#import "Question.h"
#import "QuestionOwner.h"

@implementation QuestionServiceResponseObjectMapping


-(RKObjectMapping*)objectMapping {
    
    RKObjectMapping *rootMapping = [RKObjectMapping mappingForClass:[QuestionServiceResponse class]];
    
    [rootMapping addAttributeMappingsFromDictionary:@{@"quota_max": @"max",
                                                      @"quota_remaining": @"remaining"}];
    
    RKObjectMapping *questionMapping = [RKObjectMapping mappingForClass:[Question class]];
    [questionMapping addAttributeMappingsFromDictionary:@{@"title":@"title",
                                                          @"score":@"votes",
                                                          @"answer_count":@"answers",
                                                          @"view_count":@"views",
                                                         @"tags":@"tags",
                                                         @"link":@"link"}];
    
    RKObjectMapping *ownerQuestionMapping = [RKObjectMapping mappingForClass:[QuestionOwner class]];
    [ownerQuestionMapping addAttributeMappingsFromDictionary:@{@"display_name":@"name",
                                                          @"profile_image":@"imageUrl",
                                                          @"reputation":@"reputation",
                                                          @"link":@"link"}];
    
    [questionMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"owner" toKeyPath:@"owner" withMapping:ownerQuestionMapping]];

    
    [rootMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"items" toKeyPath:@"items" withMapping:questionMapping]];
    
    return rootMapping;
}

@end
