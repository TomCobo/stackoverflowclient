//
//  QuestionsDataService.m
//  StackoverflowClient
//
//  Created by T Cobo Martinez on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "QuestionsDataService.h"
#import "QuestionServiceResponseObjectMapping.h"
#import "SearchURL.h"
#import "HttpRequetsManager.h"

@implementation QuestionsDataService

-(void)fetchQuestionsWithParams:(SearchParams*)params completionBlock:(void(^)(NSError * error, QuestionServiceResponse* response)) completedBlock {
    
    QuestionServiceResponseObjectMapping* objectMappingResponse =  [QuestionServiceResponseObjectMapping new];
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:objectMappingResponse.objectMapping method:RKRequestMethodGET pathPattern:nil keyPath:nil statusCodes:nil];
    
    
    SearchURL* searchUrl = [[SearchURL alloc] initWithEndpoint:@"questions" andSearhParams:params];

    HttpRequetsManager* requestManager = [HttpRequetsManager new];
    [requestManager requestWithURL:[searchUrl findURL]  andResponseDescriptor:@[responseDescriptor] completionBlock:^(NSError *error, id response) {
        completedBlock(error, response);
    }];
    
}


@end
