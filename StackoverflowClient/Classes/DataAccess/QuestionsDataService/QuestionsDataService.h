//
//  QuestionsDataService.h
//  StackoverflowClient
//
//  Created by T Cobo Martinez on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QuestionServiceResponse.h"
#import "SearchParams.h"

@interface QuestionsDataService : NSObject

-(void)fetchQuestionsWithParams:(SearchParams*)params completionBlock:(void(^)(NSError * error, QuestionServiceResponse* response)) completedBlock;

@end
