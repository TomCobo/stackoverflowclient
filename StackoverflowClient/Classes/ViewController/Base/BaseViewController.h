//
//  BaseViewController.h
//  StackoverflowClient
//
//  Created by Tomas Cobo on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

@end
