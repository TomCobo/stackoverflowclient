//
//  DetailViewController.h
//  StackoverflowClient
//
//  Created by Tomas Cobo on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "BaseViewController.h"
#import "DetailRootView.h"
#import "SearchParams.h"

@interface DetailViewController : BaseViewController

- (instancetype)initWithParams:(SearchParams*)params;

@end
