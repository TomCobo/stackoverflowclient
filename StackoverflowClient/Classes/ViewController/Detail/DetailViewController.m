//
//  DetailViewController.m
//  StackoverflowClient
//
//  Created by Tomas Cobo on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "DetailViewController.h"
#import "QuestionsDataService.h"

@interface DetailViewController() {
    DetailRootView* _rootView;
    SearchParams* _params;
}

@end

@implementation DetailViewController

- (instancetype)initWithParams:(SearchParams*)params {
    self = [super init];
    if (self) {
        _params = params;
    }
    return self;
}

#pragma mark - ViewController Lifecycle

- (void)loadView {
    [super loadView];
    self.view = self.rootView;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self performRequest];
}

-(void)performRequest {
    QuestionsDataService* manager = [QuestionsDataService new];
    __weak __typeof__(self) weakSelf = self;
    
    [manager fetchQuestionsWithParams:_params completionBlock:^(NSError *error, QuestionServiceResponse* response) {
        if (error == nil && response != nil) {
            weakSelf.rootView.questions = response.items;
        } else {
            [weakSelf.rootView showMessageInfoError];
        }
        
    }];
}

#pragma mark - Subviews

-(DetailRootView*)rootView {
    if (_rootView == nil) {
        _rootView = [DetailRootView new];
    }
    return _rootView;
}

- (void)dealloc {
    NSLog(@"DetailViewController dealloc");
}


@end
