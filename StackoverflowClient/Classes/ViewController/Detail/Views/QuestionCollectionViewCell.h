//
//  QuestionCollectionViewCell.h
//  StackoverflowClient
//
//  Created by T Cobo Martinez on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Question.h"

@interface QuestionCollectionViewCell : UICollectionViewCell

@property (nonatomic)Question* question;

@end
