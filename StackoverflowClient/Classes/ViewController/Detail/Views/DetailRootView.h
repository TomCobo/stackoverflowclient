//
//  DetailRootView.h
//  StackoverflowClient
//
//  Created by Tomas Cobo on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "BaseView.h"
#import "Question.h"

@interface DetailRootView : BaseView

@property(nonatomic) NSArray <Question*>* questions;
- (void)showMessageInfoError;

@end
