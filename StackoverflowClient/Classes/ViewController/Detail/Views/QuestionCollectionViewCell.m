//
//  QuestionCollectionViewCell.m
//  StackoverflowClient
//
//  Created by T Cobo Martinez on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "QuestionCollectionViewCell.h"
#import "UIView+FLKAutoLayout.h"

@interface QuestionCollectionViewCell() {
    UILabel* _titleLabel;
    UILabel* _votesLabel;
    UILabel* _answerLabel;
    UILabel* _viewsLabel;
    UILabel* _tagsLabel;
    UILabel* _creatorNameLabel;
    UILabel* _creationDateLabel;
}

@end

@implementation QuestionCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupSubviews];
        [self setupAutolayout];
    }
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.titleLabel.text = nil;
    self.votesLabel.text = nil;
    self.answerLabel.text = nil;
    self.viewsLabel.text = nil;
    self.tagsLabel.text = nil;
    self.creatorNameLabel.text = nil;
    self.creationDateLabel.text = nil;
}

-(void)setQuestion:(Question *)question {

    self.titleLabel.attributedText = [self attributedTitle:question.title];
    self.votesLabel.text = [NSString stringWithFormat:NSLocalizedString(@"detail_question_votes", comment:""),(long)question.votes];
    self.answerLabel.text = [NSString stringWithFormat:NSLocalizedString(@"detail_question_answers", comment:""),(long)question.answers];
    self.viewsLabel.text = [NSString stringWithFormat:NSLocalizedString(@"detail_question_views", comment:""),(long)question.views];
    
    self.creatorNameLabel.text = [NSString stringWithFormat:NSLocalizedString(@"detail_question_created_by", comment:""),question.owner.name];
    self.creationDateLabel.text = [NSString stringWithFormat:@"%@",question.friendlyCreationDate];
    
    self.tagsLabel.text = [question.tags componentsJoinedByString:@", "];
}

-(NSMutableAttributedString*)attributedTitle:(NSString*)title {
    NSMutableAttributedString * attributedTitle = [[NSMutableAttributedString alloc] initWithData:[title dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes: nil error:nil];
    
    [attributedTitle addAttributes:@{ NSForegroundColorAttributeName: [UIColor blueColor], NSFontAttributeName: [UIFont boldSystemFontOfSize:14]} range:NSMakeRange(0, attributedTitle.length)];
    return attributedTitle;
}

#pragma mark - setup subviews

-(void)setupSubviews {
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.titleLabel];
    [self addSubview:self.votesLabel];
    [self addSubview:self.answerLabel];
    [self addSubview:self.viewsLabel];
    [self addSubview:self.tagsLabel];
    [self addSubview:self.creatorNameLabel];
    [self addSubview:self.creationDateLabel];
}

-(void)setupAutolayout {
    [self.titleLabel alignLeading:@"10" trailing:@"-10" toView:self];
    [self.titleLabel alignTopEdgeWithView:self predicate:@"10"];
    
    [self.votesLabel alignLeadingEdgeWithView:self.titleLabel predicate:nil];
    [self.votesLabel alignAttribute:NSLayoutAttributeBottom toAttribute:NSLayoutAttributeTop ofView:self.answerLabel predicate:@"-5"];
    
    [self.answerLabel alignLeadingEdgeWithView:self.titleLabel predicate:nil];
    [self.answerLabel alignAttribute:NSLayoutAttributeBottom toAttribute:NSLayoutAttributeTop ofView:self.viewsLabel predicate:@"-5"];

    [self.viewsLabel alignLeadingEdgeWithView:self.titleLabel predicate:nil];
    [self.viewsLabel alignAttribute:NSLayoutAttributeBottom toAttribute:NSLayoutAttributeTop ofView:self.tagsLabel predicate:@"-10"];
    
    [self.creatorNameLabel alignTrailingEdgeWithView:self.titleLabel predicate:nil];
    [self.creatorNameLabel alignAttribute:NSLayoutAttributeBottom toAttribute:NSLayoutAttributeTop ofView:self.creationDateLabel predicate:@"-5"];
    
    [self.creationDateLabel alignTrailingEdgeWithView:self.titleLabel predicate:nil];
    [self.creationDateLabel alignAttribute:NSLayoutAttributeBottom toAttribute:NSLayoutAttributeTop ofView:self.tagsLabel predicate:@"-10"];
    
    [self.tagsLabel alignLeadingEdgeWithView:self.titleLabel predicate:nil];
    [self.tagsLabel alignTrailingEdgeWithView:self.titleLabel predicate:nil];
    [self.tagsLabel alignBottomEdgeWithView:self predicate:@"-10"];
}

#pragma mark - subviews

- (UILabel*) titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [UILabel new];
        _titleLabel.numberOfLines = 2;
    }
    return _titleLabel;
}

- (UILabel*) votesLabel {
    if (_votesLabel == nil) {
        _votesLabel = [UILabel new];
        [_votesLabel setFont:[UIFont systemFontOfSize:10]];
    }
    return _votesLabel;
}

- (UILabel*) answerLabel {
    if (_answerLabel == nil) {
        _answerLabel = [UILabel new];
        [_answerLabel setFont:[UIFont systemFontOfSize:10]];
    }
    return _answerLabel;
}

- (UILabel*) viewsLabel {
    if (_viewsLabel == nil) {
        _viewsLabel = [UILabel new];
        [_viewsLabel setFont:[UIFont systemFontOfSize:10]];
    }
    return _viewsLabel;
}

- (UILabel*) tagsLabel {
    if (_tagsLabel == nil) {
        _tagsLabel = [UILabel new];
        _tagsLabel.numberOfLines = 2;
        _tagsLabel.textColor = [UIColor blueColor];
        [_tagsLabel setFont:[UIFont systemFontOfSize:10]];
    }
    return _tagsLabel;
}

- (UILabel*) creatorNameLabel {
    if (_creatorNameLabel == nil) {
        _creatorNameLabel = [UILabel new];
        _creatorNameLabel.textAlignment = NSTextAlignmentRight;
        _creatorNameLabel.textColor = [UIColor grayColor];
        [_creatorNameLabel setFont:[UIFont boldSystemFontOfSize:10]];
    }
    return _creatorNameLabel;
}

- (UILabel*) creationDateLabel {
    if (_creationDateLabel == nil) {
        _creationDateLabel = [UILabel new];
        _creationDateLabel.textColor = [UIColor blueColor];
        _creationDateLabel.textAlignment = NSTextAlignmentRight;
        [_creationDateLabel setFont:[UIFont systemFontOfSize:10]];
    }
    return _creationDateLabel;
}


@end
