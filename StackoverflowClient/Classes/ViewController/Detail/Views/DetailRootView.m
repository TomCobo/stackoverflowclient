//
//  DetailRootView.m
//  StackoverflowClient
//
//  Created by Tomas Cobo on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "DetailRootView.h"
#import "QuestionCollectionViewDataSource.h"
#import "QuestionCollectionViewCell.h"
#import "StackClientCollectionView.h"


@interface DetailRootView() {
    UIActivityIndicatorView* _loadingIndicator;
    UILabel* _messageRespondeInfo;
    StackClientCollectionView* _collectionView;
    QuestionCollectionViewDataSource* _questionsDataSource;
}

@end

@implementation DetailRootView

#pragma mark - public

-(void)setQuestions:(NSArray<Question *> *)questions {
    
    [self.loadingIndicator stopAnimating];
    
    if (questions.count > 0) {
        self.questionsDataSource.questions = questions;
        [self reloadAndShowCollectionView];
    }
    else {
        [self showMessageInfoNoResults];
    }
    
}

- (void)showMessageInfoError {
    [self.loadingIndicator stopAnimating];
    self.messageRespondeInfo.text = NSLocalizedString(@"detail_question_error", comment:"");
}

#pragma mark - private

- (void)reloadAndShowCollectionView {
    [self.collectionView refresh];
}

- (void)showMessageInfoNoResults {
    self.messageRespondeInfo.text = NSLocalizedString(@"detail_question_no_results", comment:"");
}

#pragma mark - setup subviews

-(void)setupSubviews {
    self.backgroundColor = [UIColor grayColor];
    [self addSubview:self.loadingIndicator];
    [self addSubview:self.messageRespondeInfo];
    [self addSubview:self.collectionView];
}

-(void)setupAutolayout {
    
    [self.loadingIndicator alignCenterWithView:self];
    
    [self.messageRespondeInfo alignLeading:@"20" trailing:@"-20" toView:self];
    [self.messageRespondeInfo alignCenterYWithView:self predicate:nil];
    
    [self.collectionView alignTop:@"64" leading:@"0" bottom:@"0" trailing:@"0" toView:self];

}

#pragma mark - subviews

- (UIActivityIndicatorView *)loadingIndicator {
    if (_loadingIndicator == nil) {
        _loadingIndicator = [UIActivityIndicatorView new];
        [_loadingIndicator startAnimating];
        _loadingIndicator.hidesWhenStopped = YES;
    }
    return _loadingIndicator;
}

- (UILabel*)messageRespondeInfo {
    if (_messageRespondeInfo == nil) {
        _messageRespondeInfo = [UILabel new];
        _messageRespondeInfo.textColor = [UIColor whiteColor];
        _messageRespondeInfo.textAlignment = NSTextAlignmentCenter;
        _messageRespondeInfo.numberOfLines = 0;
    }
    return _messageRespondeInfo;
}

- (StackClientCollectionView*) collectionView {
    if (_collectionView == nil) {
        _collectionView = [[StackClientCollectionView alloc] initWithDataSource:self.questionsDataSource];
    }
    return _collectionView;
}

#pragma mark - data source

- (QuestionCollectionViewDataSource*)questionsDataSource {
    if (_questionsDataSource == nil) {
        _questionsDataSource = [[QuestionCollectionViewDataSource alloc] init];
        _questionsDataSource.cellClass = [QuestionCollectionViewCell class];
    }
    return _questionsDataSource;
}


@end
