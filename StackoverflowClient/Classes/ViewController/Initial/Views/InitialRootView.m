//
//  InitialRootView.m
//  StackoverflowClient
//
//  Created by Tomas Cobo on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "InitialRootView.h"

@interface InitialRootView() {
    UILabel* _titleLabel;
    UITextField* _taggedTextField;
    UIButton* _searchButton;
}

@end

@implementation InitialRootView


#pragma mark - setup subviews

-(void)setupSubviews {
    self.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:self.titleLabel];
    [self addSubview:self.taggedTextField];
    [self addSubview:self.searchButton];
}

-(void)setupAutolayout {
    
    [self.titleLabel alignLeading:@"20" trailing:@"-20" toView:self];
    [self.titleLabel alignTopEdgeWithView:self predicate:@"200"];
    
    [self.taggedTextField alignLeading:@"0" trailing:@"0" toView:self.titleLabel];
    [self.taggedTextField alignAttribute:NSLayoutAttributeTop toAttribute:NSLayoutAttributeBottom ofView:self.titleLabel predicate:@"20"];
    [self.taggedTextField constrainHeight:@"40"];
    
    [self.searchButton alignAttribute:NSLayoutAttributeTop toAttribute:NSLayoutAttributeBottom ofView:self.taggedTextField predicate:@"20"];
    [self.searchButton alignAttribute:NSLayoutAttributeTrailing toAttribute:NSLayoutAttributeTrailing ofView:self.taggedTextField predicate:nil];
}

#pragma mark - subviews

- (UILabel*)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [UILabel new];
        _titleLabel.text = NSLocalizedString(@"initial_text_description_title", comment:"");
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _titleLabel.numberOfLines = 0;
    }
    return _titleLabel;
}

-(UITextField*)taggedTextField {
    if (_taggedTextField == nil) {
        _taggedTextField = [UITextField new];
        _taggedTextField.placeholder = NSLocalizedString(@"initial_tags_placeholder", comment:"");
        _taggedTextField.backgroundColor = [UIColor whiteColor];
    }
    return _taggedTextField;
}

-(UIButton*) searchButton {
    if (_searchButton == nil) {
        _searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_searchButton setTitle:NSLocalizedString(@"initial_tags_search_button_title", comment:"") forState:UIControlStateNormal];
        [_searchButton addTarget:self action:@selector(performSearch:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _searchButton;
}

#pragma mark - actions

-(void)performSearch:(UIButton*)button {
    if (self.delegate) {
        [self.delegate initialRootViewDidPerformSearchWithParams:self.searchParams];
    }
}

- (SearchParams*)searchParams {
    SearchParams* params = [SearchParams new];
    params.tagsString = self.taggedTextField.text;
    return params;
}


@end
