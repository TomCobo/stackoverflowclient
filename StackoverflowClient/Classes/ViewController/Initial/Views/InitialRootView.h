//
//  InitialRootView.h
//  StackoverflowClient
//
//  Created by Tomas Cobo on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "BaseView.h"
#import "SearchParams.h"

@protocol InitialRootViewDelegate <NSObject>

-(void)initialRootViewDidPerformSearchWithParams:(SearchParams*)params;

@end

@interface InitialRootView : BaseView

@property (nonatomic, weak) id<InitialRootViewDelegate> delegate;

@end
