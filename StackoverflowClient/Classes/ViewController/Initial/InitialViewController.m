//
//  InitialViewController.m
//  StackoverflowClient
//
//  Created by Tomas Cobo on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "InitialViewController.h"
#import "DetailViewController.h"

@interface InitialViewController() {
    InitialRootView *_rootView;
}

@end

@implementation InitialViewController

#pragma mark - ViewController Lifecycle

- (void)loadView {
    [super loadView];
    self.view = self.rootView;
    self.title = NSLocalizedString(@"initial_tags_title", comment:"");
}

#pragma mark - Subviews

-(InitialRootView*)rootView {
    if (_rootView == nil) {
        _rootView = [[InitialRootView alloc] init];
        _rootView.delegate = self;
    }
    return _rootView;
}

#pragma mark - InitialRootViewDelegate

-(void)initialRootViewDidPerformSearchWithParams:(SearchParams*)params {
    
    BaseViewController* vc = [[DetailViewController alloc] initWithParams:params];
    
    [self.navigationController pushViewController:vc animated:YES];
}



@end
