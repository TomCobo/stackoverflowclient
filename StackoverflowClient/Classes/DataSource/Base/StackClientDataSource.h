//
//  StackClientDataSource.h
//  StackoverflowClient
//
//  Created by Tomas Cobo on 23/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StackClientDataSource : NSObject <UICollectionViewDataSource>

@property(nonatomic,assign) Class cellClass;

@end
