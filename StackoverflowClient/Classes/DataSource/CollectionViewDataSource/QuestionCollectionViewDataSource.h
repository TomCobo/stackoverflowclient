//
//  QuestionCollectionViewDataSource.h
//  StackoverflowClient
//
//  Created by T Cobo Martinez on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StackClientDataSource.h"
#import "Question.h"

@interface QuestionCollectionViewDataSource : StackClientDataSource

@property(nonatomic)NSArray<Question*>* questions;

@end
