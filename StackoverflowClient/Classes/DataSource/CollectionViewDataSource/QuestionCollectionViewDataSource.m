//
//  QuestionCollectionViewDataSource.m
//  StackoverflowClient
//
//  Created by T Cobo Martinez on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "QuestionCollectionViewDataSource.h"
#import "QuestionCollectionViewCell.h"

@implementation QuestionCollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.questions.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"Cell";
    QuestionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.question = [self.questions objectAtIndex:indexPath.item];
    
    return cell;
}


@end
