//
//  AppDelegate.m
//  StackoverflowClient
//
//  Created by Tomas Cobo on 22/02/2016.
//  Copyright © 2016 Tomas Cobo. All rights reserved.
//

#import "AppDelegate.h"
#import "InitialViewController.h"

@interface AppDelegate () {
    UINavigationController* _applicationNavigationController;
}

@end

@implementation AppDelegate

-(UINavigationController *) applicationNavigationController {
    if (_applicationNavigationController == nil) {
        InitialViewController *rootViewController = [[InitialViewController alloc] init];
        _applicationNavigationController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
    }
    return _applicationNavigationController;
}

#pragma mark - App lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [self setupMainWindow];

    return YES;
}

#pragma mark - Private setup methods

- (void)setupMainWindow {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = self.applicationNavigationController;
    self.window.backgroundColor = [UIColor blackColor];
    [self.window makeKeyAndVisible];
}

@end
