# StackOverflow Client Reader #

This repository contains a simple StackOverflow client which shows the hottest questions on the site.

### Description ###

On the first screen the app shows a textfield where the user can enter the tags to filter the questions. The user can add multiple tags separated by "," or ";". After adding a few tags (optional) the user can tap search to perform the search. A new screen will be shown.

On the second screen the hottest questions will be shown if the request didn't fail and there is at least one result. Other way it will show "error/no results" messages. Each question will contains: title, number of views, score, number of questions, owner nickname and when was created with a "Friendly time description".

### Implementation ###

This project uses CocoaPods(Restkit, FLKAutolayout). Indirectly it uses an old version of AFNetworkKit.

Structure: on this occasion I have chosen MVC structure.

Some comments...

I have used base classes for UIViews, UIViewController, DataSource. Not many functionalities added on same of them, but it useful if the project grows and it is necessary to add more features.

Why I have used vars instead of properties? 
Honestly, I have used lazy var because that was what I used to use when I develop in Objective-C, but I can use an I have used both.


#### Model ####

I have used **Restkit** to parse the responses. I know is a bit "too much" for this project but I think it was the best choice to don't reinvent the wheel and avoid crashes.

#### View ####

I have used **FLKAutoLayout** to create the UI easily programmatically setting the constraints.

Nice to have:
UI elements: on some teams UI elements are created on a theme (singleton).
About numbers: on some teams numbers for the constraints are placed on UIConstant file for each UIViewController view and subviews.

#### ViewControllers ####

ViewControllers manage the requests, models and "inject" data on the UI (rootViews). So the UI element are created with params or has a few (or none) public entrances. UI Elements get the user interactions and send it back to the ViewControllers through the weak delegate property.

#### DataSources ####

DataSource contains the data for the CollectionView. 
I normally add the delegate methods in there but this time I didn't need any delegate methods for the CollectionView.


#### Test ####

I only added test for the Model classes. It would be nice add test for UI and Network requests.